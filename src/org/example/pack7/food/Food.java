package org.example.pack7.food;

public abstract class Food<Grass, Meat> {
    Grass grass;
    Meat meat;

    public Grass getGrass() {
        return grass;
    }

    public void setGrass(Grass grass) {
        this.grass = grass;
    }

    public Meat getMeat() {
        return meat;
    }

    public void setMeat(Meat meat) {
        this.meat = meat;
    }
}
