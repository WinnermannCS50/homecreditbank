package org.example.pack7;

import org.example.pack7.animals.*;
import org.example.pack7.food.Food;
import org.example.pack7.food.Grass;
import org.example.pack7.food.Meat;

public class Worker {
    /**
     * Объекты животных
     */
    Unicorn unicorn = new Unicorn();
    BatMan batMan = new BatMan();
    Bull bull = new Bull();
    Duck duck = new Duck();
    Shark shark = new Shark();
    Tiger tiger = new Tiger();

    /**
     * Объекты еды
     */
    Grass grass = new Grass();
    Meat meat = new Meat();

    /**
     * Животные едят всё подряд))
     */
    public void feed(){

        //TODO научить животных есть правильную еду!!!!!

        System.out.println(unicorn.eat(grass) instanceof Food);
        System.out.println(unicorn.eat(meat) instanceof Food);

        System.out.println(tiger.eat(grass) instanceof Food);
        System.out.println(tiger.eat(meat) instanceof Food);

    }

    /**
     * Животные подают голос
     */
    public void getVoice(){
        System.out.println(unicorn.voice());
        System.out.println(batMan.voice());
        System.out.println(bull.voice());
        System.out.println(duck.voice());
        System.out.println(shark.voice());
        System.out.println(tiger.voice());
    }
}
