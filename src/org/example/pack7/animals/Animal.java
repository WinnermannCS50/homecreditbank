package org.example.pack7.animals;

import org.example.pack7.food.Food;

/**
 * Абстрактное животное
 */
public abstract class Animal implements Fly, Run, Swim, Voice {
    public void run(){
        System.out.println("Animal is running");

    }
    public void swim(){
        System.out.println("Animal is swimming");

    }
    public void fly(){
        System.out.println("Animal is flying");

    }
    public String voice(){
        return "Animal keep silence";
    }

    public Food eat(Food food){
        return food;
    }
}
