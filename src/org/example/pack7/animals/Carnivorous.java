package org.example.pack7.animals;

import org.example.pack7.food.Food;

/**
 * Абстрактный класс Хищные животные
 * едят мясо
 * не едят траву
 */
public abstract class Carnivorous extends Animal {
    @Override
    public Food eat(Food food) {
        return super.eat(food);
    }
}
