package org.example.pack7.animals;

/**
 * Интерфейс Fly
 * описывает поведение - летать
 */
public interface Fly {
    void fly();
}
