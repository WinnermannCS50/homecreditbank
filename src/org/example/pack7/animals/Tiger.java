package org.example.pack7.animals;

/**
 * Животное Тигр
 * хищник
 */
public class Tiger extends Carnivorous implements Run, Swim, Voice{
    public String voice(){
        String voice = "Tiger is voicing: R-r-r-r";
        return voice;
    }
}
