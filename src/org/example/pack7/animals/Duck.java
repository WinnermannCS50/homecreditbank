package org.example.pack7.animals;

/**
 * Животное утка
 * травоядное
 */
public class Duck extends Herbivore implements Fly, Run, Swim, Voice{
    public String voice(){
        String voice = "Duck is voicing: Cria-cria";
        return voice;
    }
}
