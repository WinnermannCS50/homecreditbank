package org.example.pack7.animals;

/**
 * Животное Единорог
 * травоядное
 */
public class Unicorn extends Herbivore implements Run, Swim, Voice, Fly{
    public String voice(){
        String voice = "Unicorn is voicing: I-go-go";
        return voice;
    }

}
