package org.example.pack7.animals;

/**
 * Интерфейс Voice
 * описывает поведение - подавать голос
 */
public interface Voice {
    String voice();
}
