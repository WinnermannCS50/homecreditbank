package org.example.pack7.animals;

/**
 * Интерфейс Swim
 * описывает поведение - плавать
 */
public interface Swim {
    void swim();

}
