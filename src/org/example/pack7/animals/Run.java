package org.example.pack7.animals;

/**
 * Интерфейс Run
 * описывает поведение - бегать
 */
public interface Run {
    void run();
}
