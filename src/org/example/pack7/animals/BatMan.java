package org.example.pack7.animals;

/**
 * Животное BatMan
 * хищник
 */
public class BatMan extends Carnivorous implements Run, Swim, Voice, Fly{
    public String voice(){
        String voice = "BatMan is speaking: Hi! I will save you!!";
        return voice;
    }
}
