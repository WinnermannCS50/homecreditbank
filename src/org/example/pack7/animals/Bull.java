package org.example.pack7.animals;

/**
 * Животное Бык
 * травоядное
 */
public class Bull extends Herbivore implements Run, Swim, Voice{
    public String voice(){
        String voice = "Bull is voicing: Mu-mu";
        return voice;
    }
}
