package org.example.pack7.animals;

import org.example.pack7.food.Food;

/**
 * Абстрактный класс Травоядные животные
 * едят траву
 * не едят мясо
 */
public abstract class Herbivore extends Animal {
    @Override
    public Food eat(Food food) {
        return super.eat(food);
    }
}
